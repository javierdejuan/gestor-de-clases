<?php
if ($_POST["tipo_bd"] == "SQL SERVER") {
	$devolver = '<option></option>';
	$serverName = "127.0.0.1";
	$connectionInfo = array("UID"=>$_POST["usuario"], "PWD"=>$_POST["contrasena"]);
	$conn = sqlsrv_connect( $serverName, $connectionInfo);
	$sql = 'SELECT name FROM master.dbo.sysdatabases order by name';
	$reg = sqlsrv_query($conn, $sql);
	while( $row = sqlsrv_fetch_array( $reg, SQLSRV_FETCH_ASSOC) ) {
		$devolver .= '<option>'.$row['name']."</option>";
	}
	echo $devolver;
}
else {
	try {
		$devolver = '<option></option>';
        $db = new PDO( "mysql:host=127.0.0.1", $_POST["usuario"], $_POST["contrasena"]);
		$reg = $db->query( 'SHOW DATABASES' );
		while (($nomBD = $reg->fetchColumn( 0 ) ) !== false ) {
			// echo $db.'<br>';
			$devolver .= '<option>'.$nomBD.'</option>';
		}
		
    } 
	catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
	echo $devolver;

}

?>