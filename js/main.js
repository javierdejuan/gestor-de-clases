$(function() {
	$("#tipo_bd").on("change", function() {
		$.ajax({
			async: false,
			type: "POST",
			url: "getBDs.php",
			data: {
				tipo_bd: $("#tipo_bd").val(),
				usuario: $("#usuario").val(),
				contrasena: $("#contrasena").val()
			},
			success: function(data) {
				// alert(data);
				$("#nom_bd").html(data);
			}, 
			error: function(data){ 
			}
		});
	
		
	});
	
	$("#nom_bd").on("change", function() {
		$.ajax({
			async: false,
			type: "POST",
			url: "getTablas.php",
			data: {
				nom_bd: $("#nom_bd").val(),
				tipo_bd: $("#tipo_bd").val(),
				usuario: $("#usuario").val(),
				contrasena: $("#contrasena").val()
			},
			success: function(data) {
				// alert(data);
				$("#tablas").html(data);
			}, 
			error: function(data){ 
			}
		});

	
	});
	
	$("#checkAll").click(function() {
		// alert("check");
		$(".esCheckboxSelect").prop("checked", "checked");
	});
	$("#descheckAll").click(function() {
		// alert("descheck");
		$(".esCheckboxSelect").prop("checked", null);
	});
});