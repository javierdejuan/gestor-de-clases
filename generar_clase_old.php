<?php
$arrCampos = array();
	$cadInsertNom = '';
	$cadInsertVal = '".';
	
	$cadUpdate = '';
	
	foreach ($_POST["nomCampo"] as $key => $valor) {
		$campo = "seleccionar_".$valor;
		if ($_POST[$campo]) {
			$arrCampos[] = $valor;
		}
		$campo = "clavePrimaria_".$valor;
		if ($_POST[$campo]) {
			$primaryKey = $valor;
		}
	}
		
	$cadena_parametros = '';
	$cadena_insert_def = '';
	$cadena_insert_val = '';
	$cadena_update  = '';
	
	foreach ($arrCampos as $campo) {
		if ($cadena_parametros=="") {
			$cadena_parametros .= "\$".strtolower($campo);
			$cadena_insert_def .= "".$campo;
			$cadena_insert_val .= "'\".\$".strtolower($campo).".\"'";
		} 
		else {
			$cadena_parametros .= ", \$".strtolower($campo);
			$cadena_insert_def .= ", ".$campo;
			$cadena_insert_val .= ", '\".\$".strtolower($campo).".\"'";
		}
		if($cadena_update=="") {
			if($pk_index!=$campo) {
				$cadena_update .= $campo."='\".\$".strtolower($campo).".\"'";
			}
		} 
		else {
			if($pk_index != $campo) {
				$cadena_update .= ", ".$campo."='\".\$".strtolower($campo).".\"'";
			}
		}
	}
		
	$nomClase = strtolower($_POST["tablas"]);
	$nomClase = "C".ucfirst($nomClase);
	$tabla = $_POST["tablas"];
	$php="<&#63;php ";
	?>
		<pre>
<?=$php?>

	class <?=$nomClase;?> {
		var $conn = null;
		function <?=$nomClase?>($conn) {
			$this->conn = $conn;
		}
		
		// ************** Obtener registros ***********************
		function get_<?=$tabla;?>($arr_filtros, $order="<?=$primaryKey;?> ") {
			$ro = new RESPONSE_OBJECT();
			$ro->resultado = true;
			$filtros = componer_filtro ($arr_filtros);
			$query = "SELECT * FROM <?=$_POST["tablas"];?> ".$filtros." order by ".$order; 
			if( ($arr_reg = $this->conn->load($query)) != null ) {
				$ro->datos = $arr_reg;
			}  else {
				$ro->resultado = false;
				$ro->mensaje = "Error: Se ha producido un error al obtener los registros";
			}
			
			return $ro;
		}
		
		// ********************** INSERT/UPDATE ************
		function stor_<?=$tabla;?>($datos) {  // recibe un array asociativo
			$ro = new RESPONSE_OBJECT();
			$ro->resultado = true;
			if (sizeof($datos) != null) { 
				if ($result = $this->conn->stor($datos, "<?=$tabla;?>")) {
					$ro->id = $result;
				} else {
					$ro->resultado = false;
					$ro->mensaje = "Error al Insertar/Modificar.";
				}
			} else {
					$ro->resultado = false;
					$ro->mensaje = "Error al Insertar/Modificar. Se ha pasado un array nulo.";
			}
			return $ro;	
		}
		
		// ********************* UPDATE SIN STOR *********************
		function update_<?=$tabla?>(<?=$cadena_parametros?>) {
			$ro = new RESPONSE_OBJECT();
			$ro->resultado = true;
			if ($<?=$primaryKey?> != null) {
				$query = "update <?=$tabla?> set <?=$cadena_update?> where <?=$primaryKey;?>='".$<?=$primaryKey;?>."' ";
				if ($this->conn->update($query) == false) {
					$ro->resultado = false;
					$ro->mensaje   = "No se ha podido actualizar el objeto, contacte con su administrador";
				} else {
					$ro->id = $<?=$primaryKey;?>;
				}
			} else {
				$query = "insert into <?=$tabla?> (<?=$cadena_insert_def?>) values (<?=$cadena_insert_val?>);";
				$res = $this->conn->insert($query);
				if($res==null) {
					$ro->resultado = false;
					$ro->mensaje   = "Error: No se ha podido insertar el objeto, contacte con su administrador";
				} else {
					$ro->id = $res;
				}
			}
			return $ro;
		}
		
		// *********** OBTENER TOTAL ****************************
		function get_total_<?=$tabla?>($arr_filtros) {
			$ro = new RESPONSE_OBJECT();
			$ro->resultado = true;
			$filtros = componer_filtro ($arr_filtros);
			$query = "SELECT COUNT(*) AS TANTOS FROM <?=$tabla?> ".$filtros; 
			if (($arr_reg = $this->conn->load($query)) != null) {
				$ro->datos = $arr_reg[0]->TANTOS;
			} else {
				$ro->resultado = true;
				$ro->mensaje   = "Error: Se ha producido un error al obtener el total, ponte en contacto con tu administrador";
			}
			return $ro;
		}
		
		// ******************** DELETE **************************
		function delete_<?=$tabla;?>($<?=$primaryKey;?>){
			$ro = new RESPONSE_OBJECT();
			$ro->resultado = true;
 
			$query = "select * FROM <?=$_POST["tablas"];?> where <?=$primaryKey;?>='".<?="$".$primaryKey;?>."'";
			$arr_res = $this->conn->load($query);
			if(sizeof($arr_res) != null) {
				$query = "delete from <?=$_POST["tablas"];?> where <?=$primaryKey;?>='".<?="$".$primaryKey;?>."'";
				$res = $this->conn->remove($query);
				$ro->id = <?=$primaryKey;?>;
			} else {
				$ro->resultado = false;
				$ro->id        = $<?=$primaryKey;?>;
				$ro->mensaje = "Error: se ha producido un error al borrar el registro ".$<?=$primaryKey;?>." de la tabla <?=$tabla;?>";
			}
			return $ro;
		}
	} // class
		
<?= '?>';?>
		</pre>
	