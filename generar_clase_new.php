<?php
$arrCampos = array();
	$cadInsertNom = '';
	$cadInsertVal = '".';
	
	$cadUpdate = '';
	
	foreach ($_POST["nomCampo"] as $key => $valor) {
		$campo = "seleccionar_".$valor;
		if ($_POST[$campo]) {
			$arrCampos[] = $valor;
		}
		$campo = "clavePrimaria_".$valor;
		if ($_POST[$campo]) {
			$primaryKey = $valor;
		}
	}
		
	$cadena_parametros = '';
	$cadena_insert_def = '';
	$cadena_insert_val = '';
	$cadena_update  = '';
	
	foreach ($arrCampos as $campo) {
		if ($cadena_parametros=="") {
			$cadena_parametros .= "\$".strtolower($campo);
			$cadena_insert_def .= "".$campo;
			// $cadena_insert_val .= "'\".\$".strtolower($campo).".\"'";
			$cadena_insert_val .= ":".$campo.", ";
		} 
		else {
			$cadena_parametros .= ", \$".strtolower($campo);
			$cadena_insert_def .= ", ".$campo;
			// $cadena_insert_val .= ", '\".\$".strtolower($campo).".\"'";
			$cadena_insert_val .= ":".$campo.", ";
		}
		if($cadena_update=="") {
			if($pk_index!=$campo) {
				// $cadena_update .= $campo."='\".\$".strtolower($campo).".\"'";
				$cadena_update .= strtolower($campo)." = :".strtolower($campo).", ";
			}
		} 
		else {
			if($pk_index != $campo) {
				// $cadena_update .= ", ".$campo."='\".\$".strtolower($campo).".\"'";
				$cadena_update .= strtolower($campo)." = :".strtolower($campo).", ";
			}
		}
	}
	
	if ($cadena_insert_val !="") // quitar la �ltima coma
		$cadena_insert_val = substr($cadena_insert_val, 0, -2);
	
	if ($cadena_update != "") // quitar la �ltima coma
		$cadena_update = substr($cadena_update, 0, -2); 
	
		
	$nomClase = strtolower($_POST["tablas"]);
	$nomClase = ucfirst($nomClase);
	$tabla = $_POST["tablas"];
	$php="<&#63;php ";
	?>
	
	
	<pre>
<?=$php?>

	class <?=$nomClase;?> {
	
		var $db = null;
		function <?=$nomClase?>($db) {
			$this->db = $db;
		}
		
		/**
		 * Obtiene todos los registros de la tabla <?=$_POST["tablas"];?> 
		 * que cumplan los requisitos pasado por los filtros
		 * @param array $filters se recibe un array con 2 posiciones
		 * en la primera posici&oacute;n tenemos las keys generadas con anade_filtrado
		 * en la segunda posici&oacute;n tenemos los values en un array 
		 * del tipo [':campo'] => 'valor'
		 * @param string $order
		 */
		 function get_<?=$_POST["tablas"];?>($filters, $order="<?=$primaryKey;?> asc") {
			$ro = new Response();
			$ro->resultado = true;
			
			//preparo la query
			$filtros = prepare_filters($filters['keys']);
			$this->db->query("SELECT * FROM <?=$_POST["tablas"];?> ".$filtros." order by ".$order." ;");
			$this->db->prebind($filters['values']);
			
			//la ejecuto
			$rows = $this->db->resultset();
			
			//proceso los datos
			if(is_array($rows) and sizeof($rows) > 0 ) {
				$ro->datos = $rows;
			}  else {
				$ro->resultado = false;
				$ro->mensaje   = "Error: Se ha producido un error al obtener los registros";
			}
			
			return $ro;
		}
		
		/**
		 * Realiza un insert / update autom&aacute;ticamente
		 * en funci&oacute;n de si le pasamos el <?=$primaryKey;?> o no
		 * @param array $datos array completo con todos los campos
		 * que interactuan en la query
		 * del tipo [':campo'] => 'valor'
		 */
		function stor_<?=$_POST["tablas"];?>($datos) {  // recibe un array asociativo
			$ro = new Response();
			$ro->resultado = true;
			if (sizeof($datos) != null) { 
				if ($result = $this->db->stor($datos, "<?=$_POST["tablas"];?>")) {
					$ro->id = $this->db->lastInsertId();//obtengo el ultimo id insertado
				} else {
					$ro->resultado = false;
					$ro->mensaje   = "Error al Insertar/Modificar.";
				}
			} else {
					$ro->resultado = false;
					$ro->mensaje   = "Error al Insertar/Modificar. Se ha pasado un array nulo.";
			}
			return $ro;	
		}
		
		/**
		 * Realiza un insert / update a pelo sin funci&oacute;n stor en la tabla <?=$_POST["tablas"];?>

		 * en funci&oacute;n de si le pasamos el <?=$primaryKey;?> o no  hace update o no.
		 * @param array $datos array completo con todos los campos
		 * que tiene la tabla para insertar/actualizar 
		 * del tipo [':campo'] => 'valor'
		 */
		function update_<?=$_POST["tablas"];?>($datos) {
			$ro 			= new Response();
			$ro->resultado 	= true;
			
			if ($datos[':<?=$primaryKey;?>'] != null) {
				//preparo la query
				$this->db->query("update <?=$_POST["tablas"];?> set <?=$cadena_update;?>  where <?=$primaryKey;?> = :<?=$primaryKey;?> ;");
				$this->db->prebind($datos);
				
				//la ejecuto
				if ($this->db->execute() == false) {//si va mal
					$ro->resultado = false;
					$ro->mensaje   = "No se ha podido actualizar la tabla <?=$_POST["tablas"];?>.";
				} else {//si va bien
					$ro->id = $datos[':<?=$primaryKey;?>'];//devuelvo el id actualizado
				}
				
			} else {
				//preparo la query
				$this->db->query("insert into <?=$_POST["tablas"];?> (<?=$cadena_insert_def;?>) values (<?=$cadena_insert_val;?>) ;");
				$this->db->prebind($datos);
				//la ejecuto
				if ($this->db->execute() == false) {//va mal
					$ro->resultado = false;
					$ro->mensaje   = "Error: No se ha podido insertar la tabla <?=$_POST["tablas"];?>.";
				} else {//si va bien
					$ro->id = $this->db->lastInsertId();//obtengo el ultimo id insertado
				}
			}
			return $ro;
		}
		
		/**
		 * Obtiene el total de los registros buscados de la tabla <?=$_POST["tablas"];?>
		 
		 * que cumplan los requisitos pasado por los filtros
		 * @param array $filters se recibe un array con 2 posiciones
		 * en la primera posici&oacute;n tenemos las keys generadas con anade_filtrado
		 * en la segunda posici&oacute;n tenemos los values en un array 
		 * del tipo [':campo'] => 'valor'
		 */
		function get_total_<?=$_POST["tablas"];?>($filters) {
			$ro 			= new Response();
			$ro->resultado 	= true;
			
			//preparo la query
			$filtros = prepare_filters($filters['keys']);
			$this->db->query("SELECT * FROM <?=$_POST["tablas"];?> ".$filtros." ;");
			$this->db->prebind($filters['values']);
			
			//la ejecuto
			$this->db->resultset();
			
			//obtengo los resultados
			$ro->datos 		= array();
			$ro->datos['total'] 	= $this->db->rowCount();
			return $ro;
		}
		
		/**
		 * Borra un registro de la tabla <?=$_POST["tablas"];?>, comprobando previamente su existencia
		 * @param array $<?=$primaryKey;?> id del registro a borrar
		 */
		function delete_<?=$_POST["tablas"];?>($<?=$primaryKey;?>){
			$ro = new Response();
			$ro->resultado = true;
 
			//busco el registro en la tabla
			$this->db->query("select * FROM <?=$_POST["tablas"];?> where <?=$primaryKey;?>= :<?=$primaryKey;?> ;");
			$this->db->bind(":<?=$primaryKey;?>", $<?=$primaryKey;?>);
			$arr_res = $this->db->single();
			
			//si lo encuentro
			if(sizeof($arr_res) > 0) {
				//preparo la query
				$this->db->query("delete from <?=$_POST["tablas"];?> where <?=$primaryKey;?>= :<?=$primaryKey;?> ;");
				$this->db->bind(":<?=$primaryKey;?>", $<?=$primaryKey;?>);
				//lo borro
				$this->db->execute() ;
				//devuelvo el id del registro borrado
				$ro->id = $<?=$primaryKey;?>;
			} else {//si no lo encuento, peto
				$ro->resultado = false;
				$ro->id        = $<?=$primaryKey;?>;
				$ro->mensaje = "Error: se ha producido un error al borrar el registro ".$<?=$primaryKey;?>." de la tabla <?=$_POST["tablas"];?>";
			}
			
			return $ro;
		}
		
	
	} // class
		
<?= '?>';?>
		</pre>