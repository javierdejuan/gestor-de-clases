<?php
if ($_POST["tipo_bd"] == "SQL SERVER") {
	$serverName = "127.0.0.1";
	// echo $_POST["nom_bd"].":".$_POST["usuario"].":".$_POST["contrasena"]."<hr>";
	$connectionInfo = array("Database"=>$_POST["nom_bd"], "UID"=>$_POST["usuario"], "PWD"=>$_POST["contrasena"]);
	$conn = sqlsrv_connect($serverName, $connectionInfo);
	
	$sql = "SELECT COLUMN_NAME as nombreColumna, * FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '".$_POST["tablas"]."' ORDER BY ordinal_position";
	echo $sql."<br>";
	$reg = sqlsrv_query($conn, $sql);
}
else {
	try {
        $db = new PDO('mysql:host=127.0.0.1;port=;dbname='.$_POST["nom_bd"], $_POST["usuario"], $_POST["contrasena"], array( PDO::ATTR_PERSISTENT => false));
		$sql = "show columns from ".$_POST["nom_bd"].".".$_POST["tablas"];
		$reg = $db->query($sql);
    } 
	catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
}
if (!isset($_POST["nomCampo"])) { ?>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Gestoclase</title>
	<meta name="viewport" content="width=device-width">
		<link rel="stylesheet" href="css/estilo.css" type="text/css" media="screen" title="no title" charset="utf-8">
	</head>
	<body>
	<form method="post" action="generarTabla.php">
		<input type="hidden" name="tipo_bd" value="<?php echo $_POST["tipo_bd"];?>">
		<input type="hidden" name="nom_bd" value="<?php echo $_POST["nom_bd"];?>">
		<input type="hidden" name="usuario" value="<?php echo $_POST["usuario"];?>">
		<input type="hidden" name="contrasena" value="<?php echo $_POST["contrasena"];?>">
		<input type="hidden" name="tablas" value="<?php echo $_POST["tablas"];?>">
		<input type="hidden" name="id_tipo_gestor" value="<?php echo $_POST["id_tipo_gestor"];?>">
		<table border=0 style="width:500px;background-color:#EEEEEE;border:1px solid #000;">
			<tr><td colspan=3 align=center>
				<a id="checkAll" href="javascript:void(0);">Seleccionar todos</a>
				&nbsp;
				<a id="descheckAll" href="javascript:void(0);">Deseleccionar todos</a>
			</td></tr>
			<tr>
				<td width="80%" style="font-weight:bold;text-align:center">Campo</td>
				<td width="10%" style="font-weight:bold;text-align:center">Seleccionar</td>
				<td width="10%" style="font-weight:bold;text-align:center">Clave Primaria</td>
			</tr>
			<tr><td colspan=3><hr></td></tr>
			<?php
			if ($_POST["tipo_bd"] == "SQL SERVER") {
				while( $row = sqlsrv_fetch_array( $reg, SQLSRV_FETCH_ASSOC) ) { ?>
					<input type="hidden" name="nomCampo[]" value="<?php echo $row["nombreColumna"];?>">
					<tr>
						<td width="80%" style="text-align:center;"><?php echo $row["nombreColumna"];?></td>
						<td width="10%" style="text-align:center;"><input type="checkbox" class="esCheckboxSelect" name="seleccionar_<?php echo $row["nombreColumna"];?>"></td>
						<td width="10%" style="text-align:center;"><input type="checkbox" name="clavePrimaria_<?php echo $row["nombreColumna"];?>"></td>
					</tr>
				<?php
				}
			}
			else {
				while ($row = $reg->fetch(PDO::FETCH_NUM)) { ?>
					<input type="hidden" name="nomCampo[]" value="<?php echo $row[0];?>">
					<tr>
						<td width="80%" style="text-align:center;"><?php echo $row[0];?></td>
						<td width="10%" style="text-align:center;"><input type="checkbox" class="esCheckboxSelect" name="seleccionar_<?php echo $row[0];?>"></td>
						<td width="10%" style="text-align:center;"><input type="checkbox" name="clavePrimaria_<?php echo $row[0];?>"></td>
					</tr>
				<?php
				}
			}
		?>
			<tr><td colspan=3 style="text-align:center;"><input type="submit" value="Enviar"></td></tr>
		</table>
	</form>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>
	<?php
}
else {
	if ($_POST["id_tipo_gestor"] == 0) {
		require("generar_clase_old.php");
	}
	else {
		require("generar_clase_new.php");
	}
}
?>